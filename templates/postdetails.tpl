{* szablon Smarty *}
{extends file = 'page.tpl'}
{block name="mainContent"}
    {nocache}
        <h3>{gettext('Post details')}</h3>
        {if (empty($errors['getInput']))}
            <div class="media">
                <div class="media-left">
                    <img src="{$avatarsDir}{$loadedPost.senderAvatar|default:'sheep.ico'}" class="media-object avatar-post">
                </div>
                <div class="media-body">
                    <div>
                        <a href="profile.php?id={$loadedPost.senderId}">{$loadedPost.senderName}</a>
                        <span> {$loadedPost.postCreationDate} </span>
                    </div>
                    <div>
                        <p>{$loadedPost.postText}</p>
                    </div>
                </div>
            </div>
            <h3>{gettext('Comments')}</h3>
            <div>
                {gettext('Number of comments')}: {$numberOfComments}
            </div>
            <form action="{$thisUrl.url}" method="POST" class="clearfix">
                <div class="form-group">
                    <textarea cols="50" rows="3" name="comment" class="form-control">{$submittedComment|default:''}</textarea>
                </div>
                <input type="submit" name="submitComment" value="{gettext('Submit')}" class="btn btn-default pull-right">
            </form>
            {foreach $comments as $comment}
                <div class="media">
                    <div class="media-left">
                        <img src="{$avatarsDir}{$comment.senderAvatar|default:'sheep.ico'}" class="media-object avatar-post">
                    </div>
                    <div class="media-body">
                        <div>
                            <a href="profile.php?id={$comment.senderId}">{$comment.senderName}</a>
                            <span> {$comment.commentCreationDate} </span>
                        </div>
                        <div>
                            <p>{$comment.commentText}</p>
                        </div>
                    </div>
                </div>
            {/foreach}
        {/if}
    {/nocache}
{/block}