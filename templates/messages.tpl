{* szablon Smarty *}
{extends file = 'page.tpl'}
{block name="mainContent"}
    {nocache}
        <h3>{$folderName}</h3>
        {if ($folder != 3)}
            <div><a href="messages.php">{gettext('See all messages')}</a></div>
        {/if}
        {if ($folder != 1)}
            <div><a href="messages.php?folder=1">{gettext('See sent messages')}</a></div>
        {/if}
        {if ($folder != 2)}
            <div><a href="messages.php?folder=2">{gettext('See received messages')}</a></div>
        {/if}
        {if (empty($errors)) }
            {if !empty($messages)}
                {foreach $messages as $message}                
                    <div class="media {if ($message.messageIsNew)}message-new{/if}">
                        <div class="media-left">
                            <img src="{$avatarsDir}{$message.senderAvatar|default:'sheep.ico'}" class="media-object avatar-post">
                        </div>
                        <div class="media-body">
                            <div>
                                <a href="message.php?id={$message.messageId}">{gettext('Message sent')}: {$message.messageCreationDate}</a>
                                {if ($message.messageIsNew)}
                                    <span class="glyphicon glyphicon-info-sign"></span>
                                {/if}
                            </div>
                            <div>
                                <span>{gettext('From')}: <a href="profile.php?id={$message.senderId}">{$message.senderName}</a></span>
                                <span>{gettext('To')}: <a href="profile.php?id={$message.recipientId}">{$message.recipientName}</a></span>
                            </div>
                            <div>{$message.messageTruncatedText}</div>
                        </div>
                    </div>
                {/foreach}
            {else}
                <div class="alert alert-info">{gettext('No messages')}</div>
            {/if}
        {/if}
    {/nocache}
{/block}