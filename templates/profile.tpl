{* szablon Smarty *}
{extends file = 'page.tpl'}
{block name="mainContent"}
    {nocache}
        {if (empty($errors.getInput))}
            <h3>{$profileOwner.name}</h3>
            <div class="row profile-info">
                <div class="col-xs-4">
                    <div>
                        <img src="{$avatarsDir}{$profileOwner.avatar|default:'sheep.ico'}" class="avatar-profile">
                    </div>
                    <div>
                        <a href="avatars.php">{gettext('Change avatar')}</a>
                    </div>
                </div>
                <div class="col-xs-8">
                    <div>
                        {gettext('Name: ')} {$profileOwner.name}
                    </div>
                    <div>
                        {gettext('Email: ')} {$profileOwner.email}
                    </div>
                </div>
            </div>
            {if ($profileOwner.id == $loggedUser.id)}
                <div class="profile-links">
                    <div>
                        <a href="editprofile.php">{gettext('Edit profile')}</a>
                    </div>
                    <div>
                        <a href="delete.php">{gettext('Delete account')}</a>
                    </div>
                </div>
                <div class="profile-links">
                    <div>
                        <p>{gettext('See messages')}:</p>
                        <ul>
                            <li><a href="messages.php">{gettext('all')}</a></li>
                            <li><a href="messages.php?folder=1">{gettext('sent')}</a></li>
                            <li><a href="messages.php?folder=2">{gettext('received')}</a></li>
                        </ul>
                    </div>
                </div>
            {else}
                <div>
                    <a href="sendmessage.php?id={$profileOwner.id}">{gettext('Send message')}</a>
                </div>
            {/if}
        {/if}
        {if (!empty($posts))}
            <h3>{gettext('Posts')}</h3>
            {foreach $posts as $post}
                <div class="media">
                    <div class="media-left">
                        <img src="{$avatarsDir}{$profileOwner.avatar|default:'sheep.ico'}" class="media-object avatar-post">
                    </div>
                    <div class="media-body">
                        <div>
                            <span> {$post.postCreationDate} </span>
                            <a href="postdetails.php?id={$post.postId}">{gettext('comments')}</a>
                        </div>
                        <div>
                            <p>{$post.postText}</p>
                        </div>
                    </div>
                </div>
            {/foreach}    
        {/if}
    {/nocache}
{/block}