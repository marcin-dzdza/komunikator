{* szablon Smarty *}
{extends file = 'page.tpl'}
{block name="mainContent"}
    <h3>{gettext('Edit your profile') nocache}</h3>
    <form action="{$thisUrl.url nocache}" method="POST">
        <fieldset class="clearfix">
            <legend>{gettext('Change your name') nocache}</legend>
            <div class="form-group">
                <label class="control-label" for="oldname">
                    {gettext('Old name') nocache}: 
                </label>
                <div>
                    <p class="form-control-static" id="oldname">
                        {$loggedUser.name nocache}
                    </p>
                </div>                
            </div>
            <div class="form-group">
                <label for="newname">
                    {gettext('Change to') nocache}: 
                </label>
                <input type="text" name="username" class="form-control" id="newname" value="{$submittedName|default:'' nocache}">
            </div>
            <input type="submit" name="submitName" value="{gettext('Change') nocache}" class="btn btn-default pull-right">
        </fieldset>
        <fieldset class="clearfix">
            <legend>{gettext('Change your password') nocache}</legend>
            <div class="form-group">
                <label for="oldPassword">
                    {gettext('Old password') nocache}: 
                </label>
                <input type="password" name="oldPassword" class="form-control" id="oldPassword">
            </div>
            <div class="form-group">
                <label for="newPassword">
                    <p>{gettext('New password') nocache}: </p>
                    <small class="text-muted">{gettext('At least 6 characters, including 1 digit, 1 uppercase letter and 1 lowercase letter') nocache}</small>
                </label>
                <input type="password" name="newPassword" class="form-control" id="newPassword">
            </div>
            <input type="submit" name="submitPassword" value="{gettext('Change') nocache}" class="btn btn-default pull-right">
        </fieldset>
    </form>    
{/block}