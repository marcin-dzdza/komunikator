{* szablon Smarty *}
{extends file = 'page.tpl'}
{block name="mainContent"}
    {nocache}
        <h3>{gettext('Send message')}</h3>
        {if empty($errors.getInput)}
            <div>
                {gettext('To')}: {$recipient.name}
            </div>
            <form action="{$thisUrl.url}" method="POST" class="clearfix">
                <div class="form-group">
                    <textarea cols="50" rows="3" name="message" class="form-control">{$submittedMessage|default:''}</textarea>
                </div>
                <input type="submit" name="submitMessage" value="{gettext('Send')}" class="btn btn-default pull-right">
            </form>
            <div>
                <a href="profile.php?id={$recipient.id}">{gettext('Recipient\'s profile')}</a>
            </div>
        {/if}
    {/nocache}
{/block}