{* szablon Smarty *}
{extends file = 'page.tpl'}
{block name="mainContent"}
    <h3>{gettext('Deleting account') nocache}</h3>
    <p>{gettext('Do you really want to delete your account?') nocache}</p>
    <div class="alert alert-warning">
        <span class="glyphicon glyphicon-alert"></span>
        <span>{gettext('If you delete your account, all your posts, comments and messages will be lost.') nocache}</span>
    </div>
    <form action="{$thisUrl.url nocache}" method="POST" class="form-group">
        <div class="form-group">
            <label for="password">
                {gettext('Password') nocache}: 
            </label>
            <input type="password" name="password" class="form-control" id="password">
        </div>
        <input type="submit" name="nie" value="{gettext('No, don\'t delete it') nocache}" class="btn btn-default">
        <input type="submit" name="tak" value="{gettext('Yes, delete it') nocache}" class="btn btn-default">
    </form> 
{/block}