{* szablon Smarty *}
{extends file = 'page.tpl'}
{block name="mainContent"}
    <h3>{gettext('Choose your avatar') nocache}</h3>
    <form action="{$thisUrl.url nocache}" method="POST">
        <div class="input-group row">
            <div class="col-xs-9">
                {nocache}
                    {foreach $avatars as $avatar}
                        <label>
                            <img src="{$avatarsDir}{$avatar}" class="avatar-profile">
                            <input type="radio" name="avatar" value="{$avatar}" class="form-control" {if ($avatar == $loggedUser.avatar)}checked="checked"{/if}>
                        </label>
                    {/foreach}
                {/nocache}
            </div>
            <div class="col-xs-3">
                <input type="submit" value="{gettext('Select') nocache}" name="submitAvatar" class="btn btn-default" data-spy="affix" data-offset-top="0">
            </div>
        </div>
    </form>
{/block}