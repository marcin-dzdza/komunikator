{* szablon Smarty *}
{extends file = 'page.tpl'}
{block name="mainContent"}
    <h3>{gettext('Submit a post') nocache}</h3>
    <form action="{$thisUrl.url nocache}" method="POST" class="clearfix">
        <div class="form-group">
            <textarea cols="50" rows="3" name="post" class="form-control">{$submittedPost|default:'' nocache}</textarea>
        </div>
        <input type="submit" name="submitPost" value="{gettext('Submit') nocache}" class="btn btn-default pull-right">
    </form>
    <h3>{gettext('Latest posts') nocache}</h3>
    {nocache}
        {foreach $posts as $post}
            <div class="media">
                <div class="media-left">
                    <img src="{$avatarsDir}{$post.senderAvatar|default:'sheep.ico'}" class="media-object avatar-post">
                </div>
                <div class="media-body">
                    <div>
                        <a href="profile.php?id={$post.senderId}">{$post.senderName}</a>
                        <span> {$post.postCreationDate} </span>
                        <a href="postdetails.php?id={$post.postId}">{gettext('comments')}</a>
                    </div>
                    <div>
                        <p>{$post.postText}</p>
                    </div>
                </div>
            </div>
        {/foreach}
    {/nocache}
{/block}