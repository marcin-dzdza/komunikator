{* szablon Smarty *}
{extends file = 'page.tpl'}
{block name="mainContent"}
    {nocache}
        {if (empty($errors))}
            <div class="media">
                <div class="media-left">
                    <img src="{$avatarsDir}{$message.senderAvatar|default:'sheep.ico'}" class="media-object avatar-post">
                </div>
                <div class="media-body">
                    <div>
                        {gettext('Message sent')} {$message.creationDate}
                    </div>
                    <div>
                        <span>{gettext('From')}: <a href="profile.php?id={$message.senderId}">{$message.senderName}</a></span>
                        <span>{gettext('To')}: <a href="profile.php?id={$message.recipientId}">{$message.recipientName}</a></span>
                    </div>
                    <div>{$message.text}</div>
                </div>
            </div>
        {/if}
    {/nocache}
{/block}