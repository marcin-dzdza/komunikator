{* szablon Smarty *}
{extends file = 'page.tpl'}
{block name="mainContent"}
    <h3>{gettext('Register') nocache}</h3>
    <form action="{$thisUrl.url nocache}" method="POST" class="clearfix">
        <div class="form-group">
            <label for="username">
                {gettext('name') nocache}
            </label>
            <input type="text" name="username" id="username" class="form-control" value="{$submittedUsername|default:'' nocache}">
        </div>
        <div class="form-group">
            <label for="email">
                {gettext('email') nocache}
            </label>
            <input type="text" name="email" id="email" class="form-control" value="{$submittedEmail|default:'' nocache}">
        </div>
        <div class="form-group">    
            <label for="password">
                <p>{gettext('password') nocache}</p>
                <small class="text-muted">{gettext('At least 6 characters, including 1 digit, 1 uppercase letter and 1 lowercase letter') nocache}</small>
            </label>
            <input type="password" name="password" id="password" class="form-control">
        </div>
        <input type="submit" value="{gettext('Register') nocache}" class="btn btn-default pull-right">
    </form>
    <div>
        <hr>
        <p>{gettext('Already have an account?') nocache}</p>
        <a href="login.php">{gettext('Log in') nocache}</a>
    </div>
{/block}