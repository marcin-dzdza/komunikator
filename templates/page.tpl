{* szablon Smarty *}
{* header*}
<!DOCTYPE html>
<html lang="{$language nocache}">
<head>
    <title>{$pageTitle nocache}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/global.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container-fluid">
    <div class="row content">
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-6">
                    <h1>{$sitename}</h1>
                </div>
                {* blok zmiany języka *}
                <div class="col-sm-6">
                    <ul class="list-inline">
                        {nocache}
                            {foreach $availableLanguages as $availableLanguage}
                                {if ($availableLanguage != $language)}
                                    <li><a href="{$thisUrl.url}{$thisUrl.nextParamDelimit}lang={$availableLanguage}">{$availableLanguage}</a></li>
                                {else}
                                    <li><strong>{$availableLanguage}</strong></li>
                                {/if}
                            {/foreach}
                        {/nocache}
                    </ul>
                </div>
            </div>
            {nocache}
                {* górne menu widoczne po zalogowaniu *}
                {if (isset($loggedUser))}
                    <div>
                        <p>{sprintf(gettext('Welcome, %s!'), $loggedUser.name)}</p>
                        <nav class="navbar navbar-default">
                            <div class="navbar-header">
                              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                              </button>
                              <a class="navbar-brand" href="index.php"><span class="glyphicon glyphicon-home"></span> {gettext('Home')}</a>
                            </div>
                            <div class="collapse navbar-collapse" id="myNavbar">
                              <ul class="nav navbar-nav">
                                <li><a href="profile.php"><span class="glyphicon glyphicon-user"></span> {gettext('Your profile')}</a></li>
                                <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> {gettext('Log out')}</a></li>
                              </ul>
                            </div>
                        </nav>
                    </div>
                {/if}
                {* blok z komunikatami, m.in. o błędach *}
                {foreach $errors as $error}
                    <div class="alert alert-danger alert-dismissable">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <span class="glyphicon glyphicon-alert"></span>
                      {$error}
                    </div>
                {/foreach}
                {foreach $notices as $notice}
                    <div class="alert alert-success alert-dismissable">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      {$notice}
                    </div>
                {/foreach}
            {/nocache}
            {*custom content, zdefiniowany w szablonach dziedziczących *}
            {block name="mainContent"}{/block}
            {* paginacja (jeśli strona wyświetla listę elementów) *}
            {nocache}
                {if (count($pagination) > 1)}
                    <div class="text-center">
                        <ul class="pagination">
                        {foreach $pagination as $page}
                            {if ($page == $currentPage)}
                                <li class="active"><a>{$page}</a></li>
                            {else if ($page == '')}
                                <li class="disabled"><a>...</a></li>
                            {else}
                                <li><a href="{$thisUrlWithoutPage.url}{$thisUrlWithoutPage.nextParamDelimit}page={$page}">{$page}</a></li>
                            {/if}
                        {/foreach}
                        </ul>
                    </div>
                {/if}
            {/nocache}
        {* footer *}
        </div>
        <div class="col-sm-3"></div>
    </div>
    <footer class="row footer">
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
            <div class="footer">
                <p class="text-muted">Copyright Marcin Dżdża. {gettext('Avatars created by:') nocache} <a href="http://www.iconarchive.com/show/animal-icons-by-martin-berube.html" rel="nofollow">Martin Berube</a></p>
            </div>
        </div>
        <div class="col-sm-3"></div>    
    </footer>
</div>
</body>
</html>