{* szablon Smarty *}
{extends file = 'page.tpl'}
{block name="mainContent"}
    <h3>{gettext('Log in') nocache}</h3>
    <form action="{$thisUrl.url nocache}" method="POST" class="clearfix">
        <div>
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <input id="email" type="text" class="form-control" name="email" placeholder="{gettext('Email') nocache}">
            </div>
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                <input id="password" type="password" class="form-control" name="password" placeholder="{gettext('Password') nocache}">
            </div>
        </div>
        <input type="submit" value="{gettext('Log in') nocache}" class="btn btn-default pull-right button-margin">
    </form>
    <div>
        <hr>
        <p>{gettext('No account?') nocache}</p>
        <a href="register.php">{gettext('Register now') nocache}</a>
    </div>
{/block}