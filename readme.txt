Wykonałem cały projekt i trochę go rozbudowałem. Zrobiłem 3 wersje językowe przy użyciu gettexta. Dodałem prostą strukturę MVC, do View użyłem Smarty Templating Language. Dodałem avatary.

1. zrzut bazy danych jest w katalogu głównym: komunikator.sql

2. wpisz odpowiednie dane do pliku src/config.php

3. strona wykorzystuje bibliotekę Smarty, którą trzeba zainstalować (http://www.smarty.net/download). Lokalizację biblioteki należy podać w src/config.php

4. strona wykorzystuje gettext extension, który musi być włączony w PHP. Jeśli tłumaczenie nie działa: powodem mogą być symbole języków określone w controllers/PageController.php, zmienna $validLanguages. Język musi być określony dokładnie w takiej formie, jak w systemie (u mnie: "pl_PL.utf8"). Aby sprawdzić, jak jest w systemie, należy wpisać w terminalu "locale -a". Aby zainstalować brakujący język, "sudo locale-gen fr_FR.UTF8".
