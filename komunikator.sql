-- MySQL dump 10.13  Distrib 5.7.18, for Linux (x86_64)
--
-- Host: localhost    Database: komunikator
-- ------------------------------------------------------
-- Server version	5.7.18-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Comments`
--

DROP TABLE IF EXISTS `Comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `text` varchar(140) COLLATE utf8_polish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Comments_ibfk_1` (`post_id`),
  CONSTRAINT `Comments_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `Posts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Comments`
--

LOCK TABLES `Comments` WRITE;
/*!40000 ALTER TABLE `Comments` DISABLE KEYS */;
INSERT INTO `Comments` VALUES (1,6,6,'2017-04-16 00:49:37','gdgd'),(2,6,6,'2017-04-16 01:05:48','gdgd'),(3,6,6,'2017-04-16 01:06:13','gdgd'),(4,6,6,'2017-04-16 01:06:49','nowy komentarz'),(5,6,6,'2017-04-16 01:07:13','<strong>komentarz</strong>'),(6,9,8,'2017-04-18 01:16:18','komentuję swój własny wpis'),(7,9,8,'2017-04-18 01:16:29','drugi komentarz mojego wpisu'),(8,9,6,'2017-04-18 01:16:51','ale masz fajne komentarze'),(9,9,6,'2017-04-18 01:22:21','nowy komentarz'),(10,9,12,'2017-04-21 00:28:40','ale fajny wpis'),(11,13,12,'2017-04-21 15:22:16','bardzo mi się podoba ten wpis'),(12,9,22,'2017-04-22 09:25:53','nowy komentarz'),(13,9,26,'2017-04-30 01:36:41','komentarz nr 1'),(14,9,26,'2017-04-30 01:36:45','komentarz nr 2'),(15,9,26,'2017-04-30 01:36:52','komentarz nr 3'),(16,9,26,'2017-04-30 01:36:55','komentarz nr 4'),(17,9,26,'2017-04-30 01:36:58','komentarz nr 5'),(18,9,26,'2017-04-30 01:37:00','komentarz nr 6'),(19,9,26,'2017-04-30 01:37:06','komentarz nr 7'),(20,9,26,'2017-04-30 01:37:09','komentarz nr 8'),(21,9,26,'2017-04-30 01:37:11','komentarz nr 9'),(22,9,26,'2017-04-30 01:37:15','komentarz nr 10'),(23,9,26,'2017-04-30 01:37:19','komentarz nr 11');
/*!40000 ALTER TABLE `Comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Messages`
--

DROP TABLE IF EXISTS `Messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(140) COLLATE utf8_polish_ci DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `recipient_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Messages_ibfk_1` (`sender_id`),
  KEY `Messages_ibfk_2` (`recipient_id`),
  CONSTRAINT `Messages_ibfk_1` FOREIGN KEY (`sender_id`) REFERENCES `Users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `Messages_ibfk_2` FOREIGN KEY (`recipient_id`) REFERENCES `Users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Messages`
--

LOCK TABLES `Messages` WRITE;
/*!40000 ALTER TABLE `Messages` DISABLE KEYS */;
INSERT INTO `Messages` VALUES (1,'Bleble','2017-04-17 18:34:41',0,6,5),(2,'<strong>wiadomość</strong>','2017-04-17 18:37:09',0,6,5),(3,'prywatna wiadomość','2017-04-18 01:17:04',1,9,6),(4,'cześć patryk','2017-04-18 01:17:18',0,9,8),(5,'co u ciebie?','2017-04-18 01:22:50',1,9,8),(6,'To jest dłuższa wiadomość od Kasi, która może wymagać skrócenia, bo jest za długa','2017-04-18 02:20:44',1,9,6),(7,'nieodczytana wiadomość','2017-04-18 02:48:07',1,6,9),(8,'nowa wiadomość, ma być nieodebrana','2017-04-19 14:30:40',1,6,9),(9,'Cześć patryk, co u ciebie?','2017-04-20 23:49:06',0,9,8),(10,'To na razie','2017-04-20 23:49:43',1,9,8),(11,'wiadomość do augustyna','2017-04-21 10:26:00',1,9,12),(12,'wiadomość od augustyna','2017-04-21 10:26:46',1,12,9),(13,'Co u ciebie?','2017-04-21 15:22:39',0,13,12),(14,'nowa wiadomość','2017-04-21 15:27:33',0,13,12),(15,'Wiesz, że awatary już działają?','2017-04-22 10:20:30',1,22,9),(16,'wiadomość testująca pagecontroller','2017-04-26 15:16:54',0,9,22),(17,'cześć','2017-04-28 14:19:09',0,9,22),(18,'cześć zelda','2017-05-01 14:21:06',0,9,22);
/*!40000 ALTER TABLE `Messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Posts`
--

DROP TABLE IF EXISTS `Posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `text` varchar(140) COLLATE utf8_polish_ci DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Tweets_ibfk_1` (`user_id`),
  CONSTRAINT `Posts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Posts`
--

LOCK TABLES `Posts` WRITE;
/*!40000 ALTER TABLE `Posts` DISABLE KEYS */;
INSERT INTO `Posts` VALUES (1,6,'To jest pierwszy tweet.','2017-04-15 17:09:43'),(2,6,'drugi tweet','2017-04-15 17:11:00'),(3,6,'fkslajsdlfa','2017-04-15 17:13:58'),(4,6,'fsdfsdf','2017-04-15 17:16:00'),(5,6,'<strong>to jest wiadomość z pogrubieniem</strong>','2017-04-15 18:00:32'),(6,6,'<?php echo \'to jest treść z php\' ?>','2017-04-15 18:00:53'),(7,8,'nowa wiadomość','2017-04-18 01:10:58'),(8,9,'moja pierwsza wiadomość','2017-04-18 01:16:01'),(9,9,'nowy tweet','2017-04-18 01:21:12'),(10,9,'nowy tweet','2017-04-18 01:21:32'),(11,9,'nowy tweet','2017-04-18 01:21:45'),(12,9,'wpis dodany po ustawieniu smarty','2017-04-20 00:57:59'),(13,12,'świetne wpisy','2017-04-21 10:26:20'),(14,13,'Ale marna pogoda','2017-04-21 15:16:15'),(15,13,'<strong>testuję bezpieczeństwo</strong>','2017-04-21 15:21:26'),(16,13,'<strong>testuję bezpieczeństwo</strong>','2017-04-21 15:21:42'),(17,13,'<strong>testuję bezpieczeństwo</strong>','2017-04-21 15:22:01'),(18,9,'Testuję wygląd długiej wiadomości Testuję wygląd długiej wiadomości Testuję wygląd długiej wiadomości Testuję wygląd','2017-04-21 15:40:00'),(19,9,'Testuję wygląd długiej wiadomości Testuję wygląd długiej wiadomości Testuję wygląd długiej wiadomości Testuję wygląd','2017-04-21 15:46:51'),(20,9,'Testuję wygląd długiej wiadomości Testuję wygląd długiej wiadomości Testuję wygląd długiej wiadomości Testuję wygląd','2017-04-21 15:51:14'),(21,9,'Testuję wygląd długiej wiadomości Testuję wygląd długiej wiadomości Testuję wygląd długiej wiadomości Testuję wygląd','2017-04-21 15:53:19'),(22,9,'Testuję wygląd długiej wiadomości Testuję wygląd długiej wiadomości Testuję wygląd długiej wiadomości Testuję wygląd','2017-04-21 15:53:43'),(23,22,'awatary już działają','2017-04-22 10:20:04'),(24,9,'testuję pagecontroller','2017-04-26 14:21:26'),(25,9,'testuję gettext i pagecontroller','2017-04-26 22:22:45'),(26,9,'Do tego wpisu dodaję dużo komentarzy','2017-04-30 01:36:19'),(27,9,'popisuję się','2017-05-01 14:17:41'),(28,27,'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX','2017-05-01 14:25:16'),(29,27,'<strong>fjsof</stron>','2017-05-01 14:26:40'),(30,9,'hjh','2017-05-01 16:47:40');
/*!40000 ALTER TABLE `Posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `hash_pass` varchar(60) CHARACTER SET utf8 NOT NULL,
  `avatar` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `language` varchar(2) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (4,'user.zmiana@email.com','userPoZmianie','$2y$10$sWfE9QLwH5IZAkaSw4bN0.meQbErA223x5/lATo0vCXOnYPhJmyWm','','en'),(5,'user3@email.com','user3','$2y$10$7/CMGvpai7lCQeN50Ck2oeUiOFuNXM9HLC1.aFH4eE/QJpMf1XJvK','','pl'),(6,'user5@email.com','tomek','$2y$10$VxRHbZBehGF73NymjZQPZuTNRv.SStY8KV5zrmKWMlI7PxBOy5LGa','bat.ico','en'),(7,'piotr@email.com','piotr','$2y$10$Dxcizp.KD3xapgGjudC2Y.ECAj0dLCLoZn4GO.S.HcOCQF0YFViiK','','pl'),(8,'patryk@op.pl','patryk','$2y$10$PjmefV0bOt7F2kBI/OFvfO1ZlDyCYN4hwgeIOSjj73ACJN2CzJ8xi','','en'),(9,'kasia@wp.pl','Kasia Nowak','$2y$10$JaT.KCn6RJ3Tmoosi9hscu.tZqMPLGPFx2yN.pwmZ83mrkZwFAxLu','bear.ico','fr'),(12,'augustyn@wp.pl','Augustyn','$2y$10$t2Yg1CucwyeI5FTOIQrVxuqfXdLTEFyYqjrE5hi0P2st5uQzhVjf6','fish.ico','en'),(13,'zenon@gmail.com','Zenon','$2y$10$SRtBQxhAjiDFowd9rbSPwOgofkUNDh2ftZIuh.xzo.FM36vVbG4kW','pig.ico','pl'),(14,'krystyna@op.pl','Krystyna','$2y$10$D6YpjubW.NA2Mw6eZH9Lkul6hjmhj/QDatT.9PtRlSje1u3K66Sry','','en'),(15,'mikolaj@op.pl','Mikołaj','$2y$10$1.707rrWhsIeo7Dt24fr0OLANiCRBXMWieNb2uCYdYxT24QVYa.ka','','pl'),(16,'robert@op.pl','robert','$2y$10$Lc6e6Ub6GTSETmmGe7WX8uZXuZM25pE1Lv7P3E45o0.jRCjbsb9Ny','','en'),(22,'zelda@op.pl','zelda','$2y$10$lheY5KSHI2c/MaR9koyou.QWS0JaIkB8lXCIESh8ujeJiUQinSpdi','frog.ico','pl'),(23,'wojciech@op.pl','Wojciech','$2y$10$ys/tLbaF057xlVNso8nH.OAFGzi3XQM7gWBn8RW.iyJAuuQ3Sryd2','','pl'),(24,'basia@op.pl','gdgf','$2y$10$fU/Qqk7AZFSIqd9RbTxTwuqiDNc8FzxmHtWDTHQjK7J/AGswN47yW','','en'),(25,'ferdynand@op.pl','ferdynand','$2y$10$stqvRZN6n0QjSuM2vbOoD.z7kF9YY.ruqYhJw2FEnX3JH0Ytbhcgu','','pl'),(26,'obama@op.pl','Obama','$2y$10$aivK4qdoqB9jed/4Aql73.bzVJaAHweNjhO6lnZ4WJvs.zxXjtt0u','','en'),(27,'artur@op.pl','Artur','$2y$10$LhTp4O16jtcumFpPbM8BYe2GLlnU9QFw2vsmSmBUBM.faRqI6UgHy','','fr');
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-04  1:32:57
