<?php
class Message
{
    private $id;
    private $text;
    private $creationDate;
    private $isRead;
    private $senderId;
    private $recipientId;
    
    public function __construct() {
        $this->id = -1;
        $this->text = "";
        $this->creationDate = "";
        $this->isRead = 0;
        $this->senderId = -1;
        $this->recipientId = -1;
    }
    public function getId()
    {
        return $this->id;
    }

    public function getText()
    {
        return $this->text;
    }
    
    public function getTruncatedText()
    {
        if (mb_strlen($this->text) > 30) {
            return mb_substr($this->getText(),0,30) . '...';
        }
        return $this->getText();
    }

    public function getCreationDate()
    {
        return $this->creationDate;
    }

    public function getIsRead()
    {
        return $this->isRead;
    }

    public function getSenderId()
    {
        return $this->senderId;
    }

    public function getRecipientId()
    {
        return $this->recipientId;
    }

    public function setText($text)
    {
        $this->text = $text;
    }

    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    }

    public function setIsRead($isRead)
    {
        $this->isRead = $isRead;
    }

    public function setSenderId($senderId)
    {
        $this->senderId = $senderId;
    }

    public function setRecipientId($recipientId)
    {
        $this->recipientId = $recipientId;
    }
    
    static public function loadMessageById(PDO $conn,$id)
    {
        $stmt = $conn->prepare('SELECT id AS message_id, sender_id, recipient_id, text, creation_date, is_read FROM Messages WHERE id=:id LIMIT 1');
        $result = $stmt->execute(['id' => $id]);
        if ($result == true && $stmt->rowCount() > 0 ) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $loadedMessage = new Message;
            $loadedMessage->id = $row['message_id'];
            $loadedMessage->setText($row['text']);
            $loadedMessage->setSenderId($row['sender_id']);
            $loadedMessage->setRecipientId($row['recipient_id']);
            $loadedMessage->setCreationDate($row['creation_date']);
            $loadedMessage->setIsRead($row['is_read']);
            return $loadedMessage;
        }
        return null;
    }    

    static public function loadAllMessagesByUserId(PDO $conn,$id, $folder = '3')
    {
        #folder oznacza, które wiadomości wczytać: 1. sent, 2. received, 3. all (każda inna wartość niż 1 i 2 zostanie potraktowana jako all)
        $sql = 'SELECT id AS message_id, sender_id, recipient_id, text, creation_date, is_read FROM Messages WHERE ';
        if ($folder == 1) {
            $sql .= 'sender_id=:id';
        } else if ($folder == 2) {
            $sql .= 'recipient_id=:id';
        } else {
            $sql .= '(sender_id=:id) OR (recipient_id=:id)';
        }
        $sql .= ' ORDER BY creation_date DESC';
        $stmt = $conn->prepare($sql);
        $result = $stmt->execute(['id' => $id]);
        $ret = [];
        if ($result == true && $stmt->rowCount() > 0 ) {
            foreach ($stmt as $row) {
                $loadedMessage = new Message;
                $loadedMessage->id = $row['message_id'];
                $loadedMessage->setText($row['text']);
                $loadedMessage->setSenderId($row['sender_id']);
                $loadedMessage->setRecipientId($row['recipient_id']);
                $loadedMessage->setCreationDate($row['creation_date']);
                $loadedMessage->setIsRead($row['is_read']);
                $ret[] = $loadedMessage;               
            }
        }
        return $ret;
    }

    static public function loadBatchOfMessagesByUserId(PDO $conn,$id, $folder, $limit, $offset = 0)
    {
        #folder oznacza, które wiadomości wczytać: 1. sent, 2. received, 3. all (każda inna wartość niż 1 i 2 zostanie potraktowana jako all)
        $sql = 'SELECT id AS message_id, sender_id, recipient_id, text, creation_date, is_read FROM Messages WHERE ';
        if ($folder == 1) {
            $sql .= 'sender_id=:id';
        } else if ($folder == 2) {
            $sql .= 'recipient_id=:id';
        } else {
            $sql .= '(sender_id=:id) OR (recipient_id=:id)';
        }
        $sql .= " ORDER BY creation_date DESC LIMIT $limit";
        if ($offset != 0) {
            $sql .= " OFFSET $offset";
        }        
        $stmt = $conn->prepare($sql);
        $result = $stmt->execute(['id' => $id]);
        $ret = [];
        if ($result == true && $stmt->rowCount() > 0 ) {
            foreach ($stmt as $row) {
                $loadedMessage = new Message;
                $loadedMessage->id = $row['message_id'];
                $loadedMessage->setText($row['text']);
                $loadedMessage->setSenderId($row['sender_id']);
                $loadedMessage->setRecipientId($row['recipient_id']);
                $loadedMessage->setCreationDate($row['creation_date']);
                $loadedMessage->setIsRead($row['is_read']);
                $ret[] = $loadedMessage;               
            }
        }
        return $ret;
    }
    
    static public function countAllMessagesByUserId (PDO $conn, $id, $folder = 3)
    {
        $sql = 'SELECT count(*) FROM Messages WHERE ';
        if ($folder == 1) {
            $sql .= 'sender_id=:id';
        } else if ($folder == 2) {
            $sql .= 'recipient_id=:id';
        } else {
            $sql .= '(sender_id=:id) OR (recipient_id=:id)';
        }        
        $stmt = $conn->prepare($sql);
        $result = $stmt->execute(['id' => $id]);
        if ($result == true && $stmt->rowCount() > 0) {
            return $stmt->fetchColumn();
        }
        return false;        
    }
    
    public function saveToDB(PDO $conn) {
        if ($this->id == -1) {
            $stmt = $conn->prepare('INSERT INTO Messages(text, creation_date, is_read, sender_id, recipient_id) VALUES (:text, :creationDate, :isRead, :senderId, :recipientId)');
            $result = $stmt->execute([
                'text' => $this->text,
                'creationDate' => $this->creationDate,
                'isRead' => $this->isRead,
                'senderId' => $this->senderId,
                'recipientId' => $this->recipientId
                ]);
            if ($result !== false) {
                $this->id = $conn->lastInsertId();
                return true;
            }
        } else {
            $stmt = $conn->prepare('UPDATE Messages SET text=:text, creation_date=:creationDate, is_read=:isRead, sender_id=:senderId, recipient_id=:recipientId WHERE id=:id');
            $result = $stmt->execute([
                'id' => $this->id,
                'text' => $this->text,
                'creationDate' => $this->creationDate,
                'isRead' => $this->isRead,
                'senderId' => $this->senderId,
                'recipientId' => $this->recipientId
            ]);
            if ($result !== false) {
                return true;
            }
        }
        return false;
    }
}
