<?php
class Post
{
    private $id;
    private $userId;
    private $text;
    private $creationDate;
    
    public function __construct() {
        $this->id = -1;
        $this->userId = -1;
        $this->text = "";
        $this->creationDate = "";
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function getText()
    {
        return $this->text;
    }

    public function getCreationDate()
    {
        return $this->creationDate;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    public function setText($text)
    {
        $this->text = $text;
    }

    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    }

    static public function loadPostById(PDO $conn,$id)
    {
        $stmt = $conn->prepare('SELECT id, user_id, text, creation_date FROM Posts WHERE id=:id LIMIT 1');
        $result = $stmt->execute(['id' => $id]);
        if ($result == true && $stmt->rowCount() > 0 ) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $loadedPost = new Post;
            $loadedPost->id = $row['id'];
            $loadedPost->setText($row['text']);
            $loadedPost->setUserId($row['user_id']);
            $loadedPost->setCreationDate($row['creation_date']);           
            return $loadedPost;
        }
        return null;
    }
    static public function loadAllPosts (PDO $conn)
    {
        $sql = "SELECT id, user_id, text, creation_date FROM Posts ORDER BY id DESC";
        $ret = [];
        $result = $conn->query($sql);
        if ($result !== false && $result->rowCount() != 0) {
            foreach ($result as $row) {
                $loadedPost = new Post;
                $loadedPost->id = $row['id'];
                $loadedPost->setText($row['text']);
                $loadedPost->setUserId($row['user_id']);
                $loadedPost->setCreationDate($row['creation_date']); 
                $ret[] = $loadedPost;
            }
        }
        return $ret;
    }

    static public function countAllPosts (PDO $conn)
    {
        $sql = "SELECT count(*) FROM Posts";
        $result = $conn->query($sql);
        if ($result !== false && $result->rowCount() != 0) {
            return $result->fetchColumn();
        }
        return false;
    }
    
    static public function countAllPostsByUserId (PDO $conn, $userId)
    {
        $stmt = $conn->prepare("SELECT count(*) FROM Posts WHERE user_id = :userId");
        $result = $stmt->execute(['userId' => $userId]);
        if ($result == true && $stmt->rowCount() > 0) {
            return $stmt->fetchColumn();
        }
        return false;
    }

    static public function loadBatchOfPosts (PDO $conn, $limit, $offset = 0)
    {
        #Uwaga! $offset = 1 oznacza, że pierwszy wczytany wpis będzie mieć id 2
        $sql = "SELECT id, user_id, text, creation_date FROM Posts ORDER BY id DESC LIMIT $limit";
        if ($offset != 0) {
            $sql .= " OFFSET $offset";
        }
        $ret = [];
        $result = $conn->query($sql);
        if ($result !== false && $result->rowCount() != 0) {
            foreach ($result as $row) {
                $loadedPost = new Post;
                $loadedPost->id = $row['id'];
                $loadedPost->setText($row['text']);
                $loadedPost->setUserId($row['user_id']);
                $loadedPost->setCreationDate($row['creation_date']); 
                $ret[] = $loadedPost;
            }
        }
        return $ret;
    }

    static public function loadBatchOfPostsByUserId (PDO $conn, $userId, $limit, $offset = 0)
    {
        #Uwaga! $offset = 1 oznacza, że pierwszy wczytany wpis będzie mieć id 2
        $sql = "SELECT id, user_id, text, creation_date FROM Posts WHERE user_id = :userId ORDER BY id DESC LIMIT $limit";
        if ($offset != 0) {
            $sql .= " OFFSET $offset";
        }
        $stmt = $conn->prepare($sql);
        $result = $stmt->execute(['userId' => $userId]);
        $ret = [];
        if ($result == true && $stmt->rowCount() > 0) {
            foreach ($stmt as $row) {
                $loadedPost = new Post;
                $loadedPost->id = $row['id'];
                $loadedPost->setText($row['text']);
                $loadedPost->setUserId($row['user_id']);
                $loadedPost->setCreationDate($row['creation_date']); 
                $ret[] = $loadedPost;
            }
        }
        return $ret;
    }
    
    static public function loadAllPostsByUserId(PDO $conn,$id)
    {
        $stmt = $conn->prepare('SELECT id, user_id, text, creation_date FROM Posts WHERE user_id=:id ORDER BY id DESC');
        $result = $stmt->execute(['id' => $id]);
        $ret = [];
        if ($result == true && $stmt->rowCount() > 0 ) {
            foreach ($stmt as $row) {
                $loadedPost = new Post;
                $loadedPost->id = $row['id'];
                $loadedPost->setText($row['text']);
                $loadedPost->setUserId($row['user_id']);
                $loadedPost->setCreationDate($row['creation_date']);  
                $ret[] = $loadedPost;
            }
            return $ret;
        }
        return null;
    }
    
    public function saveToDB(PDO $conn) {
        if ($this->id == -1) {
            $stmt = $conn->prepare('INSERT INTO Posts(user_id, text, creation_date) VALUES (:userId, :text, :creationDate)');
            $result = $stmt->execute([
                'userId' => $this->userId,
                'text' => $this->text,
                'creationDate' => $this->creationDate
                ]);
            if ($result !== false) {
                $this->id = $conn->lastInsertId();
                return true;
            }
        }
        return false;
    }
}