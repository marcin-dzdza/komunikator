<?php
class Comment
{
    private $id;
    private $userId;
    private $postId;
    private $creationDate;
    private $text;
    
    public function __construct() {
        $this->id = -1;
        $this->userId = -1;
        $this->postId = -1;
        $this->text = "";
        $this->creationDate = "";
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function getPostId()
    {
        return $this->postId;
    }

    public function getCreationDate()
    {
        return $this->creationDate;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    public function setPostId($postId)
    {
        $this->postId = $postId;
    }

    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    }

    public function setText($text)
    {
        $this->text = $text;
    }

    static public function loadCommentById(PDO $conn,$id)
    {
        $stmt = $conn->prepare('SELECT id AS comment_id, user_id, post_id, text, creation_date FROM Comments WHERE id=:id LIMIT 1');
        $result = $stmt->execute(['id' => $id]);
        if ($result == true && $stmt->rowCount() > 0 ) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $loadedComment = new Comment();
            $loadedComment->id = $row['comment_id'];
            $loadedComment->setText($row['text']);
            $loadedComment->setUserId($row['user_id']);
            $loadedComment->setPostId($row['post_id']);
            $loadedComment->setCreationDate($row['creation_date']);          
            return $loadedComment;
        }
        return null;
    }
    
    static public function loadAllCommentsByPostId(PDO $conn,$id)
    {
        $stmt = $conn->prepare('SELECT id AS comment_id, user_id, post_id, text, creation_date FROM Comments WHERE post_id=:id ORDER BY id DESC');
        $result = $stmt->execute(['id' => $id]);
        $ret = [];
        if ($result == true && $stmt->rowCount() > 0 ) {
            foreach ($stmt as $row) {
                $loadedComment = new Comment();
                $loadedComment->id = $row['comment_id'];
                $loadedComment->setText($row['text']);
                $loadedComment->setUserId($row['user_id']);
                $loadedComment->setPostId($row['post_id']);
                $loadedComment->setCreationDate($row['creation_date']);     
                $ret[] = $loadedComment;
            }
            return $ret;
        }
        return null;
    }

    static public function countAllCommentsByPostId (PDO $conn, $postId)
    {
        $stmt = $conn->prepare("SELECT count(*) FROM Comments WHERE post_id = :postId");
        $result = $stmt->execute(['postId' => $postId]);
        if ($result == true && $stmt->rowCount() > 0) {
            return $stmt->fetchColumn();
        }
        return false;
    }

    static public function loadBatchOfCommentsByPostId (PDO $conn, $postId, $limit, $offset = 0)
    {
        #Uwaga! $offset = 1 oznacza, że pierwszy wczytany wpis będzie mieć id 2
        $sql = "SELECT id AS comment_id, user_id, post_id, text, creation_date FROM Comments WHERE post_id = :postId ORDER BY id DESC LIMIT $limit";
        if ($offset != 0) {
            $sql .= " OFFSET $offset";
        }
        $stmt = $conn->prepare($sql);
        $result = $stmt->execute(['postId' => $postId]);
        $ret = [];
        if ($result == true && $stmt->rowCount() > 0 ) {
            foreach ($stmt as $row) {
                $loadedComment = new Comment();
                $loadedComment->id = $row['comment_id'];
                $loadedComment->setText($row['text']);
                $loadedComment->setUserId($row['user_id']);
                $loadedComment->setPostId($row['post_id']);
                $loadedComment->setCreationDate($row['creation_date']);     
                $ret[] = $loadedComment;
            }
            return $ret;
        }
        return null;
    }
    
    static public function deleteAllCommentsByUserId(PDO $conn,$id)
    {
        $stmt = $conn->prepare('DELETE FROM Comments WHERE user_id = :id');
        $result = $stmt->execute(['id' => $id]);
        return $result;
    }
    
    public function saveToDB(PDO $conn) {
        if ($this->id == -1) {
            $stmt = $conn->prepare('INSERT INTO Comments (user_id, post_id, text, creation_date) VALUES (:userId, :postId, :text, :creationDate)');
            $result = $stmt->execute([
                'userId' => $this->userId,
                'postId' => $this->postId,
                'text' => $this->text,
                'creationDate' => $this->creationDate
                ]);
            if ($result !== false) {
                $this->id = $conn->lastInsertId();
                return true;
            }
        }
        return false;
    }
    public function delete(PDO $conn)
    {
        if ($this->id != -1) {
            $stmt = $conn->prepare('DELETE FROM Comments WHERE id=:id');
            $result = $stmt->execute(['id' => $this->id]);
            
            if ($result === true) {
                $this->id = -1;
                return true;
            }
            return false;
        }
        return true;
    }
}