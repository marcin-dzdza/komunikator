<?php
class User
{
    private $id;
    private $username;
    private $email;
    private $hashPass;
    private $avatar;
    private $language;
    
    public function __construct() {
        $this->id = -1;
        $this->username = "";
        $this->email = "";
        $this->hashPass = "";
        $this->avatar = "";
        $this->language = "";
    }
    
    public function getId() {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getHashPass()
    {
        return $this->hashPass;
    }
    
    public function getAvatar()
    {
        return $this->avatar;
    }
    
    public function getLanguage()
    {
        return $this->language;
    }
    
    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setHashPass($hashPass)
    {
        $this->hashPass = $hashPass;
    }
    
    public function setPassword($password)
    {
        $this->hashPass = password_hash($password, PASSWORD_BCRYPT);
    }
    
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }
    
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    public function saveToDB(PDO $conn) {
        if ($this->id == -1) {
            $stmt = $conn->prepare('INSERT INTO Users(username, email, hash_pass, avatar, language) VALUES (:username, :email, :pass, :avatar, :language)');
            $result = $stmt->execute([
                'username' => $this->username,
                'email' => $this->email,
                'pass' => $this->hashPass,
                'avatar' => $this->avatar,
                'language' => $this->language
                ]);
            if ($result !== false) {
                $this->id = $conn->lastInsertId();
                return true;
            }
        } else {
            $stmt = $conn->prepare('UPDATE Users SET username=:username,email=:email,hash_pass=:hash_pass, avatar=:avatar, language=:language WHERE id=:id');
            $result = $stmt->execute([
                'username' => $this->username,
                'email' => $this->email,
                'hash_pass' => $this->hashPass,
                'avatar' => $this->avatar,
                'language'=> $this->language,
                'id' => $this->id ]
            );
            if ($result === true) {
            return true;
            }
        }
        return false;
    }
    
    static public function loadUserById(PDO $conn,$id)
    {
        $stmt = $conn->prepare('SELECT * FROM Users WHERE id=:id');
        $result = $stmt->execute(['id' => $id]);
        if ($result == true && $stmt->rowCount() > 0 ) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $loadedUser = new User;
            $loadedUser->id = $row['id'];
            $loadedUser->setUsername($row['username']);
            $loadedUser->setHashPass($row['hash_pass']);
            $loadedUser->setEmail($row['email']);
            $loadedUser->setAvatar($row['avatar']);
            $loadedUser->setLanguage($row['language']);
            return $loadedUser;
        }
        return null;
    }
    
    static public function loadAllUsers (PDO $conn)
    {
        $sql = "SELECT * FROM Users";
        $ret = [];
        $result = $conn->query($sql);
        if ($result !== false && $result->rowCount() != 0) {
            foreach ($result as $row) {
                $loadedUser = new User;
                $loadedUser->id = $row['id'];
                $loadedUser->setUsername($row['username']);
                $loadedUser->setHashPass($row['hash_pass']);
                $loadedUser->setEmail($row['email']);
                $loadedUser->setAvatar($row['avatar']);
                $loadedUser->setLanguage($row['language']);
                $ret[$loadedUser->getId()] = $loadedUser;
            }
        }
        return $ret;
    }
    
    public function delete(PDO $conn)
    {
        if ($this->id != -1) {
            $stmt = $conn->prepare('DELETE FROM Users WHERE id=:id');
            $result = $stmt->execute(['id' => $this->id]);
            
            if ($result === true) {
                $this->id = -1;
                return true;
            }
            return false;
        }
        return true;
    }

    static public function loadUserByEmail(PDO $conn,$email)
    {
        $stmt = $conn->prepare('SELECT * FROM Users WHERE email=:email');
        $result = $stmt->execute(['email' => $email]);
        if ($result == true && $stmt->rowCount() > 0 ) {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $loadedUser = new User;
            $loadedUser->id = $row['id'];
            $loadedUser->setUsername($row['username']);
            $loadedUser->setHashPass($row['hash_pass']);
            $loadedUser->setEmail($row['email']);
            $loadedUser->setAvatar($row['avatar']);
            $loadedUser->setLanguage($row['language']);
            return $loadedUser;
        }
        return null;
    }
    
    static public function login(PDO $conn,$email,$password)
    {
        $user = self::loadUserByEmail($conn, $email);
        if($user) {
            if (password_verify($password, $user->getHashPass())) {
                return $user->getId();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    static public function validateNewUsername($username)
    /*
Wyjaśnienie użytego regular expression
- ^ = początek, $ = koniec, + = dla wszystkich znaków
- w nawiasie [] wskazuję dopuszczalne znaki: myślnik, podkreślnik, spację, wszystkie cyfry (\d) i wszystkie litery unicode (\p{L})
- myślnik musi być pierwszym znakiem w nawiasie, aby uniknąć dwuznaczności
- jest ryzyko, że litera ze znakiem diakrytycznym nie zostanie rozpoznana jako litera, jeśli jest zakodowana za pomocą dwóch codepointów. Tak twierdzi źródło: http://www.regular-expressions.info/unicode.html Jednak przetestowałem  na wielu literach i nie wykryłem problemu
- poza // są flagi. u oznacza, że stosujemy unicode
     */
    {
        $isValid = true;
        $issue = '';
        $username = trim($username);
        if (mb_strlen($username) > 20) {
            $isValid = false;
            $issue = gettext('Invalid name. Name can be only 20 characters long');            
        } else if (preg_match('/^[-_ \d\p{L}]+$/u', $username) != 1) {
            $isValid = false;
            $issue = gettext('Invalid name. Name can include only letters, digits, space, dash (-) and underscore (_)');
        }
        return array(
            'isValid' => $isValid,
            'issue' => $issue
        );
    }
    
    static public function validateNewEmail($email)
    {
        $isValid = true;
        $issue = '';
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            $isValid = false;
            $issue = gettext('Invalid email');
        }
        return array(
            'isValid' => $isValid,
            'issue' => $issue
        );
    }
    
    #sprawdza, czy nowe hasło spełnia wymogi. Minimalna i maksymalna długość, musi zawierać co najmniej 1 cyfrę, 1 małą literę i 1 dużą literę, nie może zawierać 3 identycznych znaków po kolei. Uwaga: poprawne działanie zależy od bieżącego języka (locale), funkcja może nie policzyć poprawnie wielkich i małych liter, jeśli pochodzą z alfabetu innego języka.
    static public function validateNewPassword($password)
    {
        $isValid = true;
        $issue = '';
        if (mb_strlen($password) < 6) {
            $isValid = false;
            $issue = gettext('Password should be at least 6 characters long');
        } else if (mb_strlen($password) > 128) {
            $isValid = false;
            $issue = gettext('Password should be no more than 128 characters long');            
        } else {
            $characters = array(
                'digits' => 0,
                'lcLetters' => 0,
                'ucLetters' => 0
            );
            $lastThreeCharacters = array();
            for ($i = 0; $i < mb_strlen($password); $i++) {
                $char = mb_substr($password,$i,1);
                $lastThreeCharacters[] = $char;
                if (count($lastThreeCharacters) > 3) {
                    array_shift($lastThreeCharacters);
                }
                if (count($lastThreeCharacters) == 3 && count(array_unique($lastThreeCharacters)) == 1) {
                    $isValid = false;
                    $issue = gettext('Password should not include three identical characters in a row');
                    return array(
                        'isValid' => $isValid,
                        'issue' => $issue
                    );
                }
                if (is_numeric($char)) {
                    $characters['digits']++;
                } else if (mb_strtolower($char) != $char) {
                    $characters['ucLetters']++;
                } else if (mb_strtoupper($char) != $char) {
                    $characters['lcLetters']++;
                }
            }
            if (in_array(0,$characters)) {
                $isValid = false;
                $issue = gettext('Password should include at least one digit, one lowercase letter and one uppercase letter');
            }
        }
        return array(
            'isValid' => $isValid,
            'issue' => $issue
        );
    }
}