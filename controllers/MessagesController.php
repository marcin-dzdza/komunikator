<?php
require_once __DIR__ . '/PageController.php';

class MessagesController extends PageController
{
    public function __construct()
    {
        $this->accessibleFor = 'members';
        parent::__construct();
    }
    
    protected function customAction()
    {
        $this->pageTitle = gettext('Messages');
        $this->pageName = 'messages';
        require_once __DIR__ . '/../src/Message.php';
        
        #pobieram parametr GET folder, który informuje, które wiadomości mają być wyświetlone: 1: wysłane, 2: odebrane. Jeśli parametru brak lub jest błedny, wyświetlone są wszystkie i oznaczam to jako 3.
        if (!empty($_GET['folder'])) {
            switch ($_GET['folder']) {
                case 1:
                    $this->parameters['folder'] = 1;
                    $folderName = gettext('Sent messages');
                    break;
                case 2:
                    $this->parameters['folder'] = 2;
                    $folderName = gettext('Received messages');
                    break;
                default:
                    $this->parameters['folder'] = 3;
                    $folderName = gettext('All messages');
            }
        } else {
            $this->parameters['folder'] = 3;
            $folderName = gettext('All messages');            
        }
        
        #wczytuję z bazy wiadomości i powiązanych użytkowników
        $this->numberOfItems = Message::countAllMessagesByUserId($this->conn, $_SESSION['userId'], $this->parameters['folder']);
        $this->preparePagination();
        $messages = Message::loadBatchOfMessagesByUserId($this->conn, $_SESSION['userId'], $this->parameters['folder'], $this->itemsPerPage, $this->firstItemToLoad - 1);
        for ($i = 0; $i < count($messages); $i++) {
            if ($messages[$i]->getSenderId() == $_SESSION['userId']) {
                $this->users[$messages[$i]->getRecipientId()] = User::loadUserById($this->conn, $messages[$i]->getRecipientId());
            } else {
                $this->users[$messages[$i]->getSenderId()] = User::loadUserById($this->conn, $messages[$i]->getSenderId());                    
            }
        }

        #tworzę tablicę z wybranymi informacjami o wiadomościach, które będą dostępne w szablonie Smarty tpl. M.in. sprawdzam, czy wiadomość ma być wyróżniona jako nieprzeczytana.
        $messagesForSmarty = array();
        for ($i = 0; $i < (count($messages)); $i++) {
            $isNew = false;
            if ($messages[$i]->getSenderId() != $_SESSION['userId'] && $messages[$i]->getIsRead() < 1) {
                $isNew = true;
            }
            $message = [
                'messageId' => $messages[$i]->getId(),
                'messageCreationDate' => $messages[$i]->getCreationDate(),
                'messageIsNew' => $isNew,
                'messageTruncatedText' => htmlspecialchars($messages[$i]->getTruncatedText()),
                'senderId' => $messages[$i]->getSenderId(),
                'senderName' => htmlspecialchars($this->users[$messages[$i]->getSenderId()]->getUsername()),
                'senderAvatar' => $this->users[$messages[$i]->getSenderId()]->getAvatar(),
                'recipientId'=> $messages[$i]->getRecipientId(),
                'recipientName' => htmlspecialchars($this->users[$messages[$i]->getRecipientId()]->getUsername())        
            ];
            $messagesForSmarty[] = $message;
        }

        #przekazuję dane do smarty tpl
        $this->smarty->assign([
            'folder' => $this->parameters['folder'],
            'folderName' => $folderName,
            'messages' => $messagesForSmarty
        ]);
    }
}