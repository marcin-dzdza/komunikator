<?php
require_once __DIR__ . '/PageController.php';

class RegisterController extends PageController
{
    public function __construct()
    {
        $this->accessibleFor = 'visitors';
        parent::__construct();
    }
    
    protected function customAction()
    {
        $this->pageTitle = gettext('Register');
        $this->pageName = 'register';
        
        #pobieram dane z post i w razie potrzeby rejestruję użytkownika
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (!empty(trim($_POST['username']))) {
                $userName = trim($_POST['username']);
            }
            if (!empty(trim($_POST['email']))) {
                $userEmail = trim($_POST['email']);
            }
            if (!empty($_POST['password'])) {
                $userPassword = $_POST['password'];
            }
            if (isset ($userName) && isset($userEmail) && isset($userPassword)) {
                require_once __DIR__ . '/../src/User.php';
                $validation = array();
                $validation[] = User::validateNewUsername($userName);
                $validation[] = User::validateNewEmail($userEmail);
                $validation[] = User::validateNewPassword($userPassword);
                $everythingValid = true;
                for ($i = 0; $i < count($validation); $i++) {
                    if ($validation[$i]['isValid'] == false) {
                        $everythingValid = false;
                        $this->errors['register'] = $validation[$i]['issue'];
                        break;
                    }
                }
                if ($everythingValid) {
                    parent::setConnection();
                    $emailAlreadyInUse = User::loadUserByEmail($this->conn, $userEmail);
                    if (is_null($emailAlreadyInUse)) {
                        $newUser = new User();
                        $newUser->setUsername($userName);
                        $newUser->setEmail($userEmail);
                        $newUser->setPassword($userPassword);
                        $newUser->setLanguage($this->language);
                        if ($newUser->saveToDB($this->conn)) {
                            header('Location: login.php?register=1');
                            exit();
                        } else {
                            $this->errors['register'] = gettext('For unknown reasons account could not be created');
                        }
                    } else {
                        $this->errors['register'] = gettext('Email already exists');
                    }
                }
            } else {
                $this->errors['register'] = gettext('Please fill in all the fields');
            }
        }
        #przekazuję dane do smarty tpl
        if (!empty($this->errors['register'])) {
            if (!empty($userName)) {
                $this->smarty->assign('submittedUsername', htmlspecialchars($userName));
            }
            if (!empty($userEmail)) {
                $this->smarty->assign('submittedEmail', htmlspecialchars($userEmail));
            }
        }
    }
}