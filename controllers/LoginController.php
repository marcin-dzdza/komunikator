<?php
require_once __DIR__ . '/PageController.php';

class LoginController extends PageController
{
    public function __construct()
    {
        $this->accessibleFor = 'visitors';
        parent::__construct();
    }
    
    protected function customAction()
    {
        $this->pageTitle = 'Logowanie';
        $this->pageName = 'login';

        #pobieram dane z get, informujące o ukończonych operacjach: użytkownik wylogowany, konto usunięte
        if (!empty($_GET['logout'])) {
            if ($_GET['logout'] == 1) {
                $this->notices['logout'] = gettext('You have been logged out');
            }
        }
        if (!empty($_GET['delete'])) {
            if ($_GET['delete'] == 1) {
                $this->notices['delete'] = gettext('Account has been deleted');
            }
        }
        if (!empty($_GET['register'])) {
            if ($_GET['register'] == 1) {
                $this->notices['register'] = gettext('Account has been created');
                $this->parameters['register'] = 1;
            }
        }
        #pobieram dane z post i loguję użytkownika
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (!empty(trim($_POST['email'])) && !empty(($_POST['password']))) {
                $userEmail = trim($_POST['email']);
                $userPassword = $_POST['password'];
                parent::setConnection();
                require_once __DIR__ . '/../src/User.php';
                    if ($userId = User::login($this->conn,$userEmail,$userPassword)) {
                        $_SESSION['userId'] = $userId;
                        if (isset($this->parameters['register'])) {
                            header('Location: profile.php?register=1');
                            exit();
                        }
                        header('Location: index.php');
                        exit();
                    } else {
                        $this->errors['login'] = gettext('Invalid email or password');
                    }
            } else {
                $this->errors['login'] = gettext('One of the fields is empty');
            }
        }
    }
}