<?php
require_once __DIR__ . '/PageController.php';

class IndexController extends PageController
{
    public function __construct()
    {
        $this->accessibleFor = 'members';
        parent::__construct();
    }
    
    protected function customAction()
    {
        $this->pageTitle = gettext('Home');
        $this->pageName = 'index';
        require_once __DIR__ . '/../src/Post.php';
        
        #Sprawdzam dane z POST (publikacja nowego wpisu)
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (isset($_POST['submitPost'])) {
                if (!empty(trim($_POST['post']))) {
                    $submittedPost = trim($_POST['post']);
                    if (mb_strlen($submittedPost) <= 140) {
                        $newPost = new Post();
                        $newPost->setUserId($_SESSION['userId']);
                        $newPost->setText($submittedPost);
                        $newPost->setCreationDate(date('Y-m-d H:i:s'));
                        if ($newPost->saveToDB($this->conn)) {
                            $this->notices['submitPost'] = gettext('Post has been submitted');
                        } else {
                            $this->errors['submitPost'] = gettext('For unknown reasons post could not be submitted');
                        }
                        $newPost = null;
                    } else {
                        $this->errors['submitPost'] = gettext('The submitted post is too long. Length: ') . mb_strlen($submittedPost) . '. ' . gettext('Maximum length is 140 characters');
                    }
                } else {
                    $this->errors['submitPost'] = gettext('Please fill in the text field');
                }
            }
        }

        #wczytuję wpisy i ich autorów
        $this->numberOfItems = Post::countAllPosts($this->conn);
        $this->preparePagination();
        $posts = Post::loadBatchOfPosts($this->conn, $this->itemsPerPage, $this->firstItemToLoad - 1);
        for ($i = 0; $i < count($posts); $i++) {
            $this->users[$posts[$i]->getUserId()] = User::loadUserById($this->conn, $posts[$i]->getUserId());
        }
        
        #tworzę tablicę z wybranymi informacjami o wpisach, które będą dostępne w szablonie Smarty tpl.
        $postsForSmarty = array();
        for ($i = 0; $i < count($posts); $i++) {
            $post = [
                'postId' => $posts[$i]->getId(),
                'postCreationDate' => $posts[$i]->getCreationDate(),
                'postText' => htmlspecialchars($posts[$i]->getText()),
                'senderId' => $posts[$i]->getUserId(),
                'senderName' => htmlspecialchars($this->users[$posts[$i]->getUserId()]->getUserName()),
                'senderAvatar' => $this->users[$posts[$i]->getUserId()]->getAvatar()
            ];
            $postsForSmarty[] = $post;
        }

        #przekazuję dane do smarty tpl
        $this->smarty->assign('posts', $postsForSmarty);
        if (!empty($this->errors['submitPost']) && !empty($submittedPost)) {
            $this->smarty->assign('submittedPost', htmlspecialchars($submittedPost));
        }
    }
}