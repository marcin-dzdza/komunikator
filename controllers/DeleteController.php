<?php
require_once __DIR__ . '/PageController.php';

class DeleteController extends PageController
{
    public function __construct()
    {
        $this->accessibleFor = 'members';
        parent::__construct();
    }
    
    protected function customAction()
    {
        $this->pageTitle = gettext('Delete account');
        $this->pageName = 'delete';

        #pobieram dane z post i w razie potrzeby usuwam konto
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (isset($_POST['nie'])) {
                header('Location: profile.php');
                exit();
            } else if (isset($_POST['tak'])) {
                if (!empty($_POST['password'])) {
                    $userEmail = $this->users[$_SESSION['userId']]->getEmail();
                    $userPassword = $_POST['password'];
                        if (User::login($this->conn,$userEmail,$userPassword)) {
                            require_once __DIR__ . '/../src/Comment.php';
                            if (Comment::deleteAllCommentsByUserId($this->conn,$_SESSION['userId'])) {
                                if ($this->users[$_SESSION['userId']]->delete($this->conn)) {
                                    header('Location: logout.php?delete=1');
                                    exit();
                                } else {
                                    $this->errors['delete'] = 'For unknown reasons account could not be deleted';
                                }
                            } else {
                                $this->errors['delete'] = 'For unknown reasons account could not be deleted';
                            }
                        } else {
                            $this->errors['delete'] = gettext('Invalid password');
                        }
                } else {
                    $this->errors['delete'] = gettext('Please fill in the password field');
                }
            }
        }
    }
}