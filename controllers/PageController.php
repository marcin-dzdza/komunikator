<?php
class PageController
{
    const SITENAME = 'Komunikator'; #nazwa witryny
    protected $smarty; #obiekt klasy Smarty do obsługi szablonów tpl
    protected $errors; #tablica z komunikatami o błędzie
    protected $notices; #tablica z innymi komunikatami
    protected $language; #w jakim języku ma być strona (dla gettexta)
    protected $conn; #połączenie z bazą
    protected $users; #tablica z obiektami wczytanych użytkowników (id => obiekt User)
    protected $pageName; #nazwa pliku, np. profile (dla profile.php i profile.tpl)
    protected $pageTitle; #tytuł danej  podstrony, np. "Profil użytkownika" dla profile.php
    protected $parameters; #tablica z parametrami get po walidacji (np. 'id' => 15), bez parametrów, które nie powinny trafić do następnego żądania (np. logout=1)
    protected $accessibleFor; #kto może tu wejść: 'members', 'visitors', 'everyone'
    protected $validLanguagesShort; #tablica: obsługiwane języki => ich skróty
    protected $itemsPerPage = 15; #ile wpisów, wiadomości itd.  na 1 stronie
    protected $numberOfItems; #ile jest wpisów itd. w sumie na wszystkich stronach
    protected $currentPage; #którą stronę teraz wyświetlić
    protected $numberOfPages; #ile jest stron w sumie
    protected $pagination; #tablica ze stronami do wyświetlenia w paginacji (np. 1 '' 3 4 5 6 7 '' 15);
    protected $firstItemToLoad; #numer pierwszego wpisu, komentarza itd. do wczytania (w związku z paginacją)
    
    protected function __construct()
    {    
        session_start();
        if ($this->accessibleFor == 'members' && !isset($_SESSION['userId'])) {
            header('Location: login.php');
            exit();
        }
        if (!$this->accessibleFor == 'visitors' && isset($_SESSION['userId'])) {
            header('Location: index.php');
            exit();
        }
        
        require_once __DIR__ . '/../src/config.php';
        mb_internal_encoding('UTF-8');
        mb_http_output('UTF-8');
        $this->errors = array();
        $this->notices = array();
        $this->itemsPerPage = 10;
        $this->numberOfItems = 0;
        $this->currentPage = 1;
        $this->pagination = array(1);
        $this->initSmarty();
        if (isset($_SESSION['userId'])) {
            $this->setConnection();
            $this->loadLoggedUser();
        }
        $this->setLanguage();
        $this->initGettext();
        $this->customAction();
        $this->assignSmarty();
        $this->conn = null;
        $this->smarty->display($this->pageName . '.tpl');
    }

    #konfiguruję Smarty
    protected function initSmarty()
    {
        require_once(SMARTY_DIR . 'Smarty.class.php');
        $this->smarty = new Smarty();
        $this->smarty->setTemplateDir(__DIR__ . '/../templates/');
        $this->smarty->setCompileDir(__DIR__ . '/../templates_c/');
        $this->smarty->setConfigDir(__DIR__ . '/../configs/');
        $this->smarty->setCacheDir(__DIR__ . '/../cache/');
        $this->smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
        
        #poniższe opcje powinny być zakomentowane po zakończeniu budowy strony
        $this->smarty->force_compile = true;
        $this->smarty->caching = Smarty::CACHING_OFF;
    }
    
    #łączę się z bazą
    protected function setConnection()
    {
        $options = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC];
        try {
            $conn = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_DB . ";charset=utf8", DB_USER,DB_PASS,$options);
        } catch (Exception $ex) {
            echo gettext('Database connection error') . ': ' . $ex->getMessage();
        }
        $this->conn = $conn;
    }
    
    #wczytuję zalogowanego użytkownika
    protected function loadLoggedUser()
    {
        require_once __DIR__ . '/../src/User.php';
        $this->users = array();
        $this->users[$_SESSION['userId']] = User::loadUserById($this->conn, $_SESSION['userId']);
    }
        
    #główna frunkcjonalność danej podstrony, określana w klasach pochodnych
    protected function customAction()
    {
        
    }
    
    # sprawdzam, jaki powinien być język strony
    protected function setLanguage()
    {
        $validLanguages = array('en_GB.utf8', 'pl_PL.utf8', 'fr_FR.utf8');
#nazwy języków muszą odpowiadać ich nazwom w systemie, które mogą być różne (pl, pl_PL, pl_PL.utf8). Należy sprawdzić w terminalu za pomocą "locale -a", jak nazywają się języki w systemie, i podać te nazwy tutaj. Jeśli brakuje, zainstalować poprzez "sudo locale-gen fr_FR.utf8". Zakładam, że mimo różnic zawsze będą się zaczynać od dwuliterowego kodu języka. Zakładam, że w tym programie nie będzie różnych tłumaczeń dla odmian tego samego języka.
        $this->validLanguagesShort = array(); #tablica ze skróconymi nazwami wg wzoru: ['en_GB.utf8' => 'en', 'pl_PL.utf8' => 'pl', ...]. W bazie, cookies, sesji i atrybucie klasy przechowuję wersję skróconą. Pełna nazwa potrzebna jest tylko dla gettexta.
        for ($i = 0; $i < count($validLanguages); $i++) {
            $this->validLanguagesShort[$validLanguages[$i]] = mb_substr($validLanguages[$i],0,2);
        }

        #Krok 1. Najpierw szukamy języka w parametrze GET (oznacza to, że użytkownik kliknął właśnie w link służący do zmiany języka)
        if (!empty($_GET['lang'])) {
            if (in_array($_GET['lang'],$this->validLanguagesShort)) {
                $this->language = $_GET['lang'];
                #jeśli użytkownik jest zalogowany, aktualizujemy jego rekord w bazie
                if (isset($_SESSION['userId']) && isset($this->users[$_SESSION['userId']])) {
                    $this->users[$_SESSION['userId']]->setLanguage($this->language);
                    $this->users[$_SESSION['userId']]->saveToDB($this->conn);
                }
            }    
        }

        #Krok 2. Jeśli użytkownik jest zalogowany, język strony pobieramy z jego rekordu w bazie.
        if (empty($this->language) && isset($_SESSION['userId']) && isset($this->users[$_SESSION['userId']])) {
            $this->language = $this->users[$_SESSION['userId']]->getLanguage();
        }

        #Krok 3. Dalej szukamy języka w cookies
        if (empty($this->language) && !empty($_COOKIE['lang'])) {
            if (in_array($_COOKIE['lang'], $this->validLanguagesShort)) {
                $this->language = $_COOKIE['lang'];
            }
        }

        #Krok 4. Dalej szukamy języka w $_SERVER['HTTP_ACCEPT_LANGUAGE']
        if (empty($this->language) && !empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            #pobieram listę języków ze zmiennej $_SERVER['HTTP_ACCEPT_LANGUAGE']. Przykładowa wartość to: 'pl,en-US;q=0.7,en;q=0.3'. Poniższy mechanizm ma na tej podstawie utworzyć tablicę o następującej postaci: ['pl', 'en'].
            $langArray1 = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);
            if (!is_array($langArray1)) {
                $langArray1 = array($langArray1);
            }
            $langArray2 = array();
            for ($i = 0; $i < count($langArray1); $i++) {
                if (mb_strpos($langArray1[$i],';')) {
                    $langArray2 = array_merge($langArray2, explode(';',$langArray1[$i]));
                } else {
                    $langArray2[] = $langArray1[$i];
                }
            }
            $acceptLanguages = array();
            for ($i = 0; $i < count($langArray2); $i++) {
                if (mb_substr($langArray2[$i],0,2) != 'q=') {
                    $acceptLanguages[] = mb_substr($langArray2[$i],0,2);
                }
            }
            #w tym momencie mam już tablicę w postaci array ['pl', 'en'] i porównuję ją z listą języków, które strona obsługuje. Biorę pierwszą pasującą wartość
            for ($i = 0; $i < count($acceptLanguages); $i++) {
                if (in_array($acceptLanguages[$i],$this->validLanguagesShort)) {
                    $this->language = $acceptLanguages[$i];
                    break;
                }
            }
        }

        #jeśli wciąż nie udało się znaleźć języka, ustawiamy język domyślny - angielski
        if (empty($this->language)) {
            $this->language = 'en';
        }

        #po ustaleniu języka zapisujemy go do cookies.
        setcookie('lang',$this->language,time()+3600 * 24 * 30);
    }
    
    #konfiguruję gettexta
    protected function initGettext()
    {
        $langForGettext = array_search($this->language,$this->validLanguagesShort);
        putenv('LANG=' . $langForGettext);
        setlocale(LC_ALL, $langForGettext);
        $textDomain = 'messages';
        bindtextdomain($textDomain, __DIR__ . '/../locale');
        bind_textdomain_codeset($textDomain, 'UTF-8');
        textdomain($textDomain);        
    }
    
    #przekazuję wartości do szablonów Smarty tpl
    protected function assignSmarty()
    {
        $this->smarty->assign([
            'sitename' => self::SITENAME,
            'pageTitle' => $this->pageTitle,
            'thisUrl' => $this->getUrl(),
            'thisUrlWithoutPage' => $this->getUrl(['page']),
            'errors' => $this->errors,
            'notices' => $this->notices,
            'language' => $this->language,
            'availableLanguages' => array_values($this->validLanguagesShort),
            'avatarsDir' => 'img/avatars/',
            'currentPage' => $this->currentPage,
            'pagination' => $this->pagination
        ]);
        if (isset($_SESSION['userId'])) {
            $this->smarty->assign('loggedUser', [
                'id' => $_SESSION['userId'],
                'name' => $this->users[$_SESSION['userId']]->getUsername(),
                'email' => $this->users[$_SESSION['userId']]->getEmail(),
                'avatar' => $this->users[$_SESSION['userId']]->getAvatar(),
                'language' => $this->users[$_SESSION['userId']]->getLanguage()
            ]);         
        }
    }
    
    #generowanie linku do bieżącej strony razem z parametrami get. Poprzez atrybut $this->parameters można zdecydować, które parametry będą uwzględnione. Ten link jest podawany przy "<form action=""> i zmianie języka. Opcjonalny parametr $excluded może zawierać tablicę z parametrami, które mają być wykluczone (np. page do linków paginacji)
    protected function getUrl($excluded = array())
    {
        $url = $this->pageName . '.php';
        $nextParamDelimit = '?';
        if (!empty($this->parameters)) {
            foreach ($this->parameters as $key => $value) {
                if (!in_array($key, $excluded)) {
                    $url .= $nextParamDelimit . $key . '=' . $value;
                    $nextParamDelimit = '&';                    
                }
            }
        }
        return array('url' => $url, 'nextParamDelimit' => $nextParamDelimit);
    }
    
    protected function countNumberOfPages()
    {
        if ($this->numberOfItems > 0) {
            $this->numberOfPages = ceil($this->numberOfItems / $this->itemsPerPage);
        } else {
            $this->numberOfPages = 1;
        }
    }
    
    protected function setCurrentPage()
    {
        #konstruktor przypisał już domyślną stronę: 1
        if (!empty($_GET['page'])) {
            if (is_numeric($_GET['page'])) {
                if ($_GET['page'] > 0 && $_GET['page'] <= $this->numberOfPages) {
                    $this->currentPage = intval($_GET['page']);
                    $this->parameters['page'] = $this->currentPage;
                }
            }
        }
    }
    protected function preparePagination()
    {
        $this->countNumberOfPages();
        $this->setCurrentPage();
        $this->firstItemToLoad = 1;
        #konstruktor uwzględnił już stronę nr 1
        #case 1: jedna strona, brak paginacji
        if ($this-> numberOfPages < 2) {
            return false;
        }
        #case 2: paginacja obejmuje wszystkie strony: 1 2 3 4 5 6 7 8 9
        if ($this->numberOfPages <= 9) {
            for ($i = 2; $i <= $this->numberOfPages; $i++) {
                $this->pagination[] = $i;
            }
        }
        #case 3: paginacja skrócona w stylu: 1 ... 7 8 9 10 11 ... 22
        if ($this->numberOfPages > 9) {
            if ($this->currentPage - 3 > 1) {
                $this->pagination[] = '';
            }
            for ($i = ($this->currentPage - 2); $i < ($this->currentPage + 3); $i++) {
                if ($i > 1 && $i < $this->numberOfPages) {
                    $this->pagination[] = $i;
                }
            }
            if ($this->pagination[count($this->pagination) - 1] + 1 != $this->numberOfPages) {
                $this->pagination[] = '';
            }
            $this->pagination[] = $this->numberOfPages;
        }
        $this->firstItemToLoad = ($this->currentPage * $this->itemsPerPage) - $this->itemsPerPage + 1;
    }
}