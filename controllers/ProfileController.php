<?php
require_once __DIR__ . '/PageController.php';

class ProfileController extends PageController
{
    public function __construct()
    {
        $this->accessibleFor = 'members';
        parent::__construct();
    }
    
    protected function customAction()
    {
        $this->pageTitle = gettext('Profile');
        $this->pageName = 'profile';
        require_once __DIR__ . '/../src/Post.php';
        
        #pobieram dane z GET i wczytuję wskazanego użytkownika
        if (!empty($_GET['id'])) {
            if (is_numeric($_GET['id'])) {
                if ($_GET['id'] > 0) {
                    $this->parameters['id'] = $_GET['id'];
                    if ($this->parameters['id'] == $_SESSION['userId']) {
                        $userToShow = $_SESSION['userId'];
                    } else {
                        $loadedUser = User::loadUserById($this->conn, $this->parameters['id']);
                        if (is_null($loadedUser)) {
                            $this->errors['getInput'] = gettext('Invalid user ID');
                        } else {
                            $this->users[$loadedUser->getId()] = $loadedUser;
                            $userToShow = $loadedUser->getId();
                            $loadedUser = null;
                        }                
                    }
                } else {
                    $this->errors['getInput'] = gettext('Invalid user ID');
                }
            } else {
                $this->errors['getInput'] = gettext('Invalid user ID');
            }
        } else {
            $userToShow = $_SESSION['userId'];
        }
       
        #jeśli udało się wczytać użytkownika, przygotowuję profil do wyświetlenia
        if (empty($this->errors['getInput'])) {

        #przekazuję dane użytkownika do szablonu smarty tpl
            $this->smarty->assign('profileOwner',[
                'id' => $userToShow,
                'name' => $this->users[$userToShow]->getUsername(),
                'email' => $this->users[$userToShow]->getEmail(),
                'avatar' => $this->users[$userToShow]->getAvatar(),
                'language' => $this->users[$userToShow]->getLanguage()
            ]);    

        #wczytuję wpisy użytkownika i tworzę tablicę z wybranymi informacjami o wpisach, które będą dostępne w szablonie Smarty tpl.
            $this->numberOfItems = Post::countAllPostsByUserId($this->conn, $userToShow);
            $this->preparePagination();
            $posts = Post::loadBatchOfPostsByUserId($this->conn, $userToShow, $this->itemsPerPage, $this->firstItemToLoad - 1);
            $postsForSmarty = array();
            if (isset($posts[0])) {
                for ($i = 0; $i < count($posts); $i++) {
                    $post = [
                        'postId' => $posts[$i]->getId(),
                        'postCreationDate' => $posts[$i]->getCreationDate(),
                        'postText' => htmlspecialchars($posts[$i]->getText())
                    ];
                    $postsForSmarty[] = $post;
                }        
            }
            $this->smarty->assign('posts', $postsForSmarty);
        }
        
        #jeśli użytkownik został przekierowany, wyświetlam odpowiedni komunikat
        if (!empty($_GET['register'])) {
            if ($_GET['register'] == 1) {
                $this->notices['register'] = gettext('Choose an avatar and submit your first post');
            }
        }
        if (!empty($_GET['update'])) {
            if ($_GET['update'] == 'avatar') {
                $this->notices['submitAvatar'] = gettext('Avatar has been changed');
            } else if ($_GET['update'] == 'name') {
                $this->notices['submitName'] = gettext('Name has been changed');
            } else if ($_GET['update'] == 'password') {
                $this->notices['submitPassword'] = gettext('Password has been changed');
            }
        }
    }
}