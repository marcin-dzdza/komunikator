<?php
require_once __DIR__ . '/PageController.php';

class MessageController extends PageController
{
    public function __construct()
    {
        $this->accessibleFor = 'members';
        parent::__construct();
    }
    
    protected function customAction()
    {
        $this->pageTitle = gettext('Message');
        $this->pageName = 'message';
        require_once __DIR__ . '/../src/Message.php';
        
        #pobieram dane z GET i wczytuję wiadomość z bazy
        if (!empty($_GET['id'])) {
            if (is_numeric($_GET['id'])) {
                if ($_GET['id'] > 0) {
                    $this->parameters['id'] = $_GET['id'];
                    $loadedMessage = Message::loadMessageById($this->conn, $this->parameters['id']);
                    if (is_null($loadedMessage)) {
                        $this->errors['getInput'] = gettext('Invalid message ID');
                    } else {
                        if ($loadedMessage->getRecipientId() == $_SESSION['userId']) {
                            $loadedMessage->setIsRead(1);
                            $loadedMessage->saveToDB($this->conn);
                            $this->users[$loadedMessage->getSenderId()] = User::loadUserById($this->conn, $loadedMessage->getSenderId());
                        } else if ($loadedMessage->getSenderId() == $_SESSION['userId']) {
                            $loadedMessage->setIsRead(1);
                            $loadedMessage->saveToDB($this->conn);
                            $this->users[$loadedMessage->getRecipientId()] = User::loadUserById($this->conn, $loadedMessage->getRecipientId());
                        } else {
                            $this->errors['getInput'] = gettext('Invalid message ID');
                        }
                    }
                } else {
                    $this->errors['getInput'] = gettext('Invalid message ID');
                }
            } else {
                $this->errors['getInput'] = gettext('Invalid message ID');
            }
        } else {
            $this->errors['getInput'] = gettext('No message has been selected');
        }

        #przekazywanie danych do smarty tpl
        if (empty($this->errors)) {
            $this->smarty->assign('message', [
                'creationDate' => $loadedMessage->getCreationDate(),
                'senderId' => $loadedMessage->getSenderId(),
                'senderName' => htmlspecialchars($this->users[$loadedMessage->getSenderId()]->getUsername()),
                'senderAvatar' => $this->users[$loadedMessage->getSenderId()]->getAvatar(),
                'recipientId' => $loadedMessage->getRecipientId(),
                'recipientName' => htmlspecialchars($this->users[$loadedMessage->getRecipientId()]->getUsername()),
                'text' => htmlspecialchars($loadedMessage->getText())
            ]);    
        }
    }
}
