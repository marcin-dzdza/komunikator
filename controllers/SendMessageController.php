<?php
require_once __DIR__ . '/PageController.php';

class SendMessageController extends PageController
{
    public function __construct()
    {
        $this->accessibleFor = 'members';
        parent::__construct();
    }
    
    protected function customAction()
    {
        $this->pageTitle = gettext('Send a message');
        $this->pageName = 'sendmessage';
        
        #przekierowanie, jeśli parametr GET id jest pusty lub odpowiada zalogowanemu użytkownikowi
        if (empty($_GET['id'])) {
            header('Location: profile.php');
            exit();
        } else if ($_GET['id'] == $_SESSION['userId']) {
            header('Location: profile.php');
            exit();
        }

        #wczytuję klasy
        require_once __DIR__ . '/../src/Message.php';

        #pobieram dane z GET
        if (is_numeric($_GET['id'])) {
            if ($_GET['id'] > 0) {
                $this->parameters['id'] = $_GET['id'];
                $loadedUser = User::loadUserById($this->conn, $this->parameters['id']);
                if (is_null($loadedUser)) {
                    $this->errors['getInput'] = gettext('Invalid user ID');
                } else {
                    $this->users[$loadedUser->getId()] = $loadedUser;
                    $recipient = $loadedUser->getId();

                    $loadedUser = null;
                }
            } else {
                $this->errors['getInput'] = gettext('Invalid user ID');
            }
        } else {
            $this->errors['getInput'] = gettext('Invalid user ID');
        }

        #pobieram dane z POST, tworzę wiadomość, zapisuję do bazy, niszczę obiekt.
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (isset($_POST['submitMessage'])) {
                if (isset($recipient)) {
                    if (!empty(trim($_POST['message']))) {
                        $submittedMessage = trim($_POST['message']);
                        if (mb_strlen($submittedMessage) <= 140) {
                            $newMessage = new Message();
                            $newMessage->setSenderId($_SESSION['userId']);
                            $newMessage->setRecipientId($recipient);
                            $newMessage->setText($submittedMessage);
                            $newMessage->setCreationDate(date('Y-m-d H:i:s'));
                            if ($newMessage->saveToDB($this->conn)) {
                                $this->notices['submitMessage'] = gettext('Message has been sent');
                            } else {
                                $this->errors['submitMessage'] = gettext('For unknown reasons message could not be sent');
                            }
                            $newMessage = null;
                        } else {
                            $this->errors['submitMessage'] = gettext('The submitted message is too long. Length: ') . mb_strlen($submittedMessage) . '. ' . gettext('Maximum length is 140 characters');
                        }
                    } else {
                        $this->errors['submitMessage'] = gettext('Please fill in the text field');
                    }
                }
            }
        }

        #przekazuję dane do smarty tpl
        if (!empty($this->errors['submitMessage'])&& !empty($submittedMessage)) {
            $this->smarty->assign('submittedMessage', htmlspecialchars($submittedMessage));
        }
        if (empty($this->errors['getInput'])) {
            $this->smarty->assign('recipient',[
                'id' => $recipient,
                'name' => $this->users[$recipient]->getUsername(),
                'email' => $this->users[$recipient]->getEmail(),
                'avatar' => $this->users[$recipient]->getAvatar()
            ]);
        }
    }
}