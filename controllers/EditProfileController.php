<?php
require_once __DIR__ . '/PageController.php';

class EditProfileController extends PageController
{
    public function __construct()
    {
        $this->accessibleFor = 'members';
        parent::__construct();
    }
    
    protected function customAction()
    {
        $this->pageTitle = gettext('Edit profile');
        $this->pageName = 'editprofile';
        require_once __DIR__ . '/../src/Post.php';
        
        #pobieram dane z post i aktualizuję dane użytkownika
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (isset($_POST['submitName'])) {
                if (!empty($_POST['username'])) {
                    if (mb_strlen(trim($_POST['username'])) > 0) {
                        $submittedName = trim($_POST['username']);
                        $validation = User::validateNewUsername($submittedName);
                        if ($validation['isValid'] == true) {
                            $this->users[$_SESSION['userId']]->setUsername($submittedName);
                            if ($this->users[$_SESSION['userId']]->saveToDB($this->conn)) {
                                header('Location: profile.php?update=name');
                                exit();
                            } else {
                                $this->errors['submitName'] = gettext('For unknown reasons name could not be changed');
                            }                            
                        } else {
                            $this->errors['submitName'] = $validation['issue'];
                        }
                    } else {
                        $this->errors['submitName'] = gettext('Please fill in the name field');
                    }
                } else {
                    $this->errors['submitName'] = gettext('Please fill in the name field');
                }
            } else if (isset($_POST['submitPassword'])) {
                if (!empty($_POST['oldPassword']) && !empty($_POST['newPassword'])) {
                    if (User::login($this->conn,$this->users[$_SESSION['userId']]->getEmail(),$_POST['oldPassword'])) {
                        $validation = User::validateNewPassword($_POST['newPassword']);
                        if ($validation['isValid'] == true) {
                            $this->users[$_SESSION['userId']]->setPassword($_POST['newPassword']);
                            if ($this->users[$_SESSION['userId']]->saveToDB($this->conn)) {
                                header('Location: profile.php?update=password');
                                exit();
                            } else {
                                $this->errors['submitPassword'] = gettext('For unknown reasons password could not be changed');
                            }
                        } else {
                            $this->errors['submitPassword'] = $validation['issue'];
                        }
                    } else {
                        $this->errors['submitPassword'] = gettext('Old password is invalid');
                    }
                } else {
                    $this->errors['submitPassword'] = gettext('Please fill in the old and new password fields');
                }
            }
        }
        #przekazuję dane do smarty tpl
        if (isset($_POST['submitName']) && !empty($this->errors['submitName'])&& !empty($submittedName)) {
            $this->smarty->assign('submittedName', htmlspecialchars($submittedName));
        }
    }
}