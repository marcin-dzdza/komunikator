<?php
require_once __DIR__ . '/PageController.php';

class LogoutController extends PageController
{
    public function __construct()
    {
        $this->accessibleFor = 'members';
        parent::__construct();
    }
    
    protected function customAction()
    {
        if (isset($_SESSION['userId'])) {
            unset($_SESSION['userId']);
        }
        #poniżej wyjątkowo ustalam pełną ścieżkę dla header Location, ponieważ przeglądarka z nieznanych przyczyn nie akceptowała wersji skróconej
        $url = "http".(!empty($_SERVER['HTTPS'])?"s":"").
    "://".$_SERVER['SERVER_NAME'] . rtrim(dirname($_SERVER['PHP_SELF']), '/\\') . '/login.php';
        $getParam = '?logout=1';
        if (isset($_GET['delete'])) {
            if ($_GET['delete'] == 1) {
                $getParam = '?delete=1';
            }
        }
        header('Location: ' . $url . $getParam);
        exit();
    }
}