<?php
require_once __DIR__ . '/PageController.php';

class AvatarsController extends PageController
{
    public function __construct()
    {
        $this->accessibleFor = 'members';
        parent::__construct();
    }
    
    protected function customAction()
    {
        $this->pageTitle = gettext('Choose avatar');
        $this->pageName = 'avatars';

        #pobieram listę dostępnych awatarów w katalogu img/avatars
        $dir = __DIR__ . '/../img/avatars';
        $files = scandir($dir);
        $avatars = array();
        foreach ($files as $file) {
            $pathInfo = pathinfo($dir . '/' . $file);
            if ($pathInfo['extension'] == 'ico') {
                $avatars[] = $file;
            }
        }

        #sprawdzam dane z POST (formularz wyboru awatara)
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (isset($_POST['submitAvatar']) && !empty($_POST['avatar'])) {
                if (in_array($_POST['avatar'],$avatars)) {
                    if ($_POST['avatar'] != $this->users[$_SESSION['userId']]->getAvatar()) {
                        $this->users[$_SESSION['userId']]->setAvatar($_POST['avatar']);
                        if ($this->users[$_SESSION['userId']]->saveToDB($this->conn)) {
                            header('Location: profile.php?update=avatar');
                            exit();
                        } else {
                            $this->errors['submitAvatar'] = gettext('For unknown reasons avatar could not be changed');
                        }
                    } else {
                        $this->notices['submitAvatar'] = gettext('Avatar has been changed');
                    }
                } else {
                    $this->errors['submitAvatar'] = gettext('Selected avatar not found');
                }
            } else {
                $this->errors['submitAvatar'] = gettext('No avatar selected');
            }
        }

        #przekazuję dane do smarty
        $this->smarty->assign('avatars', $avatars);
    }
}