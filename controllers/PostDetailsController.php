<?php
require_once __DIR__ . '/PageController.php';

class PostDetailsController extends PageController
{
    public function __construct()
    {
        $this->accessibleFor = 'members';
        parent::__construct();
    }
    
    protected function customAction()
    {
        $this->pageTitle = gettext('Post details');
        $this->pageName = 'postdetails';
        require_once __DIR__ . '/../src/Post.php';
        require_once __DIR__ . '/../src/Comment.php';
        
        #pobieram dane z GET i wczytuję wskazany wpis
        if (!empty($_GET['id'])) {
            if (is_numeric($_GET['id'])) {
                if ($_GET['id'] > 0) {
                    $this->parameters['id'] = $_GET['id'];
                    $loadedPost = Post::loadPostById($this->conn, $this->parameters['id']);
                    if (empty($loadedPost)) {
                        $this->errors['getInput'] = gettext('Invalid post ID');
                    } else {
                        $this->users[$loadedPost->getUserId()] = User::loadUserById($this->conn, $loadedPost->getUserId());
                    }
                } else {
                    $this->errors['getInput'] = gettext('Invalid post ID');
                }
            } else {
                $this->errors['getInput'] = gettext('Invalid post ID');
            }
        } else {
            $this->errors['getInput'] = gettext('No post has been selected');
        }

        #pobieram dane z POST, tworzę komentarz, zapisuję do bazy, niszczę obiekt
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (isset($_POST['submitComment'])) {
                if (!empty(trim($_POST['comment']))) {
                    $submittedComment = trim($_POST['comment']);
                    if (mb_strlen($submittedComment) <= 140) {
                        $newComment = new Comment();
                        $newComment->setUserId($_SESSION['userId']);
                        $newComment->setPostId($loadedPost->getId());
                        $newComment->setText($submittedComment);
                        $newComment->setCreationDate(date('Y-m-d H:i:s'));
                        if ($newComment->saveToDB($this->conn)) {
                            $this->notices['submitComment'] = gettext('Comment has been submitted');
                        } else {
                            $this->errors['submitComment'] = gettext('For unknown reasons comment could not be submitted');
                        }
                        $newComment = null;
                    } else {
                        $this->errors['submitComment'] = gettext('The submitted comment is too long. Length: ') . mb_strlen($submittedComment) . '. ' . gettext('Maximum length is 140 characters');
                    }
                } else {
                    $this->errors['submitComment'] = gettext('Please fill in the text field');
                }
            }
        }

        #ładuję wszystkie komentarze do wpisu oraz ich autorów
        if (!empty($loadedPost)) {
            $this->numberOfItems = Comment::countAllCommentsByPostId($this->conn, $this->parameters['id']);
            $this->smarty->assign('numberOfComments',$this->numberOfItems);
            $this->preparePagination();
            $comments = Comment::loadBatchOfCommentsByPostId($this->conn, $this->parameters['id'], $this->itemsPerPage, $this->firstItemToLoad - 1);
            for ($i = 0; $i < count($comments); $i++) {
                $this->users[$comments[$i]->getUserId()] = User::loadUserById($this->conn, $comments[$i]->getUserId());
            }

        #przekazuję wybrane informacje o wpisie do szablonu smarty tpl
            $this->smarty->assign('loadedPost',[
                'postId' => $loadedPost->getId(),
                'postCreationDate' => $loadedPost->getCreationDate(),
                'postText' => htmlspecialchars($loadedPost->getText()),
                'senderId' => $loadedPost->getUserId(),
                'senderName' => htmlspecialchars($this->users[$loadedPost->getUserId()]->getUsername()),
                'senderAvatar' => $this->users[$loadedPost->getUserId()]->getAvatar()
            ]);

        #przekazuję treść wysłanego komentarza do szablonu tpl
            if (!empty($this->errors['submitComment']) && !empty($submittedComment)) {
                $this->smarty->assign('submittedComment', htmlspecialchars($submittedComment));
            }

        #przekazuję wybrane informacje o komentarzach do szablonu smarty tpl    
            $commentsForSmarty = array();
            for ($i = 0; $i < count($comments); $i++) {
                $comment = [
                    'commentId' => $comments[$i]->getId(),
                    'commentCreationDate' => $comments[$i]->getCreationDate(),
                    'commentText' => htmlspecialchars($comments[$i]->getText()),
                    'senderId' => $comments[$i]->getUserId(),
                    'senderName' => htmlspecialchars($this->users[$comments[$i]->getUserId()]->getUserName()),
                    'senderAvatar' => $this->users[$comments[$i]->getUserId()]->getAvatar()
                ];
                $commentsForSmarty[] = $comment;
            }
            $this->smarty->assign('comments', $commentsForSmarty);
        }
    }
}




